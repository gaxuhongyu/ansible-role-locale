ansible-role-locale
===================

Setup system default locale and other locales.

Role Variables
--------------

- `locale_locales` : list of locales to generate. The first of this list will be the system locale;
- `locale_ssh_stop_acceptenv` : boolean to make sshd stop accepting client's LANG or LC_ environment variables.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
     - role: locale
       locale_locales: [ fr_FR.UTF-8, en_US.UTF-8 ]
```

License
-------

GPLv3

Alternatives
------------

- [knopki.locale](https://galaxy.ansible.com/list#/roles/332)
- [lborguetti.system-locale](https://galaxy.ansible.com/list#/roles/3195)
- [staenker.supported-locales](https://galaxy.ansible.com/list#/roles/2124)
- [williamyeh.reset-locale](https://galaxy.ansible.com/list#/roles/2716)

